<?php
    /**
     * Created by PhpStorm.
     * User: Miguel Seuster
     * Date: 24.11.16
     * Time: 20:27
     * Copyrigt: Copyright � 2016 - 2018, Miguel
     */

    include './getID3/getid3/getid3.php';

    $rootDir = "[%%/path/to/mp3/dir/%%]";
    $fileList = readDataRecursive( $rootDir );

    if( !empty( $fileList ) ) {
        foreach( $fileList as $file ) {
            $ID3Info = getID3Tags( $file );
            if( !$ID3Info || !isset( $ID3Info[1]["id3v2"] ) ) {
                continue;
            }
            $ID3Tags = $ID3Info[1]["id3v2"];
            $current = false;
            $all     = 0;
            if( isset( $ID3Tags["part_of_a_set"] ) && isset( $ID3Tags["part_of_a_set"][0] ) ) {
                list( $current, $all )  = explode( '/', $ID3Tags["part_of_a_set"][0] );
            }
            list( $track, $tracks ) = explode( '/', $ID3Tags["track_number"][0] );
            $trackNo                = sprintf( '%02d', $track );
            if( $current !== 0 && $all > 1 ) {
                $name = $trackNo . '_CD-' . $current . '_' . cleaningName( $ID3Tags["album"][0] ) . '_' . cleaningName( $ID3Tags["title"][0] ) . '.' . $ID3Info[0];
            }
            else {
                $name = $trackNo . '_' . cleaningName( $ID3Tags["album"][0] ) . '_' . cleaningName( $ID3Tags["title"][0] ) . '.' . $ID3Info[0];
            }
            $dirName                = pathinfo( $file, PATHINFO_DIRNAME );
            print_r( "renaming $file" . "\n" );
            rename( $file, $dirName . '/' . $name );
        }
    }

    function getID3Tags( $filePath ) {
        $getID3 = new getID3();
        $analyzed = $getID3->analyze( $filePath );
        if( isset( $analyzed["tags_html"] ) && isset( $analyzed["fileformat"] ) ) {
            return array( $analyzed["fileformat"], $analyzed["tags_html"] );
        }
        return false;
    }

    function readDataRecursive( $rootDir ) {
        $files = array();
        $dir = new RecursiveDirectoryIterator( $rootDir, RecursiveDirectoryIterator::SKIP_DOTS );
        $subDir = new RecursiveIteratorIterator( $dir );
        $subDir->setMaxDepth( '4' );
        foreach( $subDir as $path => $obj ) {
            if( $obj->getExtension() == 'mp3' || $obj->getExtension() == 'm4a' ) {
                $files[] = $obj->getRealPath();
            }
        }
        return $files;
    }

    function cleaningName( $string ) {
        $string = str_replace( array( '[\', \']' ), '', $string );
        $string = preg_replace( '/\[.*\]/U', '', $string );
        $string = preg_replace( '/&(amp;)?#?[a-z0-9]+;/i', '-', $string );
        $string = htmlentities( $string, ENT_COMPAT, 'utf-8' );
        $string = preg_replace( '/&([a-z])(acute|uml|circ|grave|ring|cedil|slash|tilde|caron|lig|quot|rsquo);/i', '\\1', $string );
        $string = preg_replace( array( '/[^a-z0-9]/i', '/[-]+/' ), '-', $string );
        return strtolower( trim( $string, '-' ) );
    }